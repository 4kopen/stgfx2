/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/


#ifndef __STM_GFXDRIVER_H__
#define __STM_GFXDRIVER_H__


#include <dfb_types.h>
#include <core/state.h>
#include <core/screens.h>
#include <core/layers.h>
#include <core/surface_pool.h>

#include <linux/stm/bdisp2_ioctl.h>
#include <linux/stm/bdisp2_nodegroups.h>
#include "bdisp2/bdispII_aq_state.h"

#include "stm_types.h"
#include "bdisp_features.h"

#include "stgfx_screen.h"
#include "stgfx_primary.h"

/* buffer to store the operations in format
 * suitable to the Hw ip */
struct _nodes_buf_data {
	unsigned char *buf_p;
	int            buf_len;
	int            next_elt;
};

/* Structure for storing information about device state */
struct _STGFX2DeviceData
{
  struct stm_bdisp2_device_data stdev;

  struct _bdisp_dfb_features dfb_features;

  GraphicsDeviceInfo *device_info;

  /* nodes are stored before being forwarded to bdisp driver */
  struct _nodes_buf_data nodes_buf;
};


struct _STGFX2DriverData
{
  int fd_gfx;
  int stmfb_acceleration; /* old style stmfb or new stm-blitter driver */

  int accel_type;

  struct stm_bdisp2_driver_data stdrv;

  CoreGraphicsDevice *device;
};


#endif /* __STM_GFXDRIVER_H__ */
