/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/

#ifndef __BDISP2_OS_DIRECTFBGLIBC_H__
#define __BDISP2_OS_DIRECTFBGLIBC_H__

#include <direct/debug.h>
#include <direct/util.h>
#include <stdlib.h>

#define STM_BLITTER_DEBUG_DOMAIN D_DEBUG_DOMAIN

/*
 * STM_BLITTER_ASSERT has to be used to track only development bug likely
 * impossible situations and not wrong parameter. Could be evaluated to void
 * if debug is disabled so has to be used with and braces when needed.
 */
#define STM_BLITTER_ASSERT D_ASSERT
#define STM_BLITTER_ASSUME D_ASSUME
#define stm_blit_printw    D_WARN
#define stm_blit_printe    D_ERROR
#define stm_blit_printd    D_DEBUG_AT
#define stm_blit_printi    D_INFO


#define STM_BLITTER_N_ELEMENTS(array) D_ARRAY_SIZE(array)
#define STM_BLITTER_ABS(val)          ABS(val)
#define STM_BLITTER_MIN(a,b)          MIN(a,b)

#define __iomem
#define STM_BLITTER_RAW_WRITEL(val,addr)        *(addr) = (val)
#define STM_BLITTER_MEMSET_IO(dst,val,size)     memset(dst,val,size)
#define STM_BLITTER_MEMCPY_TO_IO(dst,src,size)  memcpy(dst,src,size)

#define flush_bdisp2_dma_area_region(a,o,s)
#define flush_bdisp2_dma_area(a)


#define bdisp2_div64_u64(d,n)   ((uint64_t) ((d) / (n)))


#define BDISP2_MB()  __asm__ __volatile__ ("" : : : "memory")
#define BDISP2_WMB() __asm__ __volatile__ ("" : : : "memory")


#define USEC_PER_SEC    1000000L
#define bdisp2_start_timer_os() \
     ({ \
       struct timespec currenttime;                              \
       clock_gettime (CLOCK_MONOTONIC, &currenttime);            \
       (((uint64_t) currenttime.tv_sec * USEC_PER_SEC) \
        + (currenttime.tv_nsec / 1000));                         \
     })


static inline uint32_t
bdisp2_get_reg (const struct stm_bdisp2_driver_data * const stdrv,
                uint16_t                             offset)
{
  return *(volatile uint32_t *) (stdrv->mmio_base + 0xa00 + offset);
}

static inline void
bdisp2_set_reg (struct stm_bdisp2_driver_data * const stdrv,
                uint16_t                       offset,
                uint32_t                       value)
{
  *(volatile uint32_t *) (stdrv->mmio_base + 0xa00 + offset) = value;
}

#endif /* __BDISP2_OS_DIRECTFBGLIBC_H__ */
