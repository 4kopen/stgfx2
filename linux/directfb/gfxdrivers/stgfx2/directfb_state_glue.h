/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/

#ifndef __DIRECTFB_STATE_GLUE_H__
#define __DIRECTFB_STATE_GLUE_H__

void bdisp_aq_StateInit (void      *driver_data,
                         void      *device_data,
                         CardState *state);
void bdisp_aq_StateDestroy (void      *driver_data,
                            void      *device_data,
                            CardState *state);

void bdisp_aq_CheckState (void                *driver_data,
                          void                *device_data,
                          CardState           *state,
                          DFBAccelerationMask  accel);

void bdisp_aq_SetState (void                *driver_data,
                        void                *device_data,
                        GraphicsDeviceFuncs *funcs,
                        CardState           *state,
                        DFBAccelerationMask   accel);

char *stgfx2_get_dfb_drawflags_string (DFBSurfaceDrawingFlags flags);
char *stgfx2_get_dfb_blitflags_string (DFBSurfaceBlittingFlags flags);
char *stgfx2_get_dfb_renderoptions_string (DFBSurfaceRenderOptions opts);


#endif /* __DIRECTFB_STATE_GLUE_H__ */
