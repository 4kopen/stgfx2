/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/


#ifndef __BDISP_FEATURES_H__
#define __BDISP_FEATURES_H__

#include <directfb.h>

struct _bdisp_dfb_features {
  DFBSurfaceDrawingFlags   dfb_drawflags;
  DFBSurfaceBlittingFlags  dfb_blitflags;
  DFBSurfaceRenderOptions  dfb_renderopts;
};


#endif /* __BDISP_FEATURES_H__ */
