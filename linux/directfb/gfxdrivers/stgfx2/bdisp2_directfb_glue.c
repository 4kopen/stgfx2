/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/

#include "config.h"

#include <sys/ioctl.h>
#include <sys/mman.h>

#include <directfb.h>
#include <core/coretypes.h>
#include <core/gfxcard.h>

#include <linux/stm/bdisp2_nodegroups.h>
#include "bdisp2/bdispII_aq_state.h"

#include "stgfx2_features.h"
#include "stm_gfxdriver.h"
#include "bdisp2/bdisp_accel.h"

#include "bdisp2_directfb_glue.h"
#include "directfb_state_glue.h"
#include "bdisp2/bdisp2_os.h"


#define offset_of(type, memb)     ((unsigned long)(&((type *)0)->memb))
#define container_of(ptr, type, member) ({ \
           const typeof( ((type *)0)->member ) *__mptr = (ptr); \
           (type *)( (char *)__mptr - offset_of(type,member) );})


int
bdisp2_engine_sync_os (struct stm_bdisp2_driver_data       * const stdrv,
                       const struct stm_bdisp2_device_data * const stdev)
{
  struct wait_cmd cmd;
  struct _STGFX2DriverData * const drvdata
    = container_of (stdrv, struct _STGFX2DriverData, stdrv);
  int ret = 0;

  cmd.wait = STM_BLITTER_W_IDLE;

  while (ioctl (drvdata->fd_gfx, STM_BLITTER_SYNC,&cmd) < 0)
     {
       if (errno == EINTR)
         continue;

       ret = errno;
       D_PERROR ("BDisp/BLT: STM_BLITTER_SYNC failed!\n" );

       break;
     }
  return ret;
}

int
bdisp2_alloc_memory_os (struct stm_bdisp2_driver_data * const stdrv,
                        struct stm_bdisp2_device_data * const stdev,
                        int                            alignment,
                        bool                           cached,
                        struct stm_bdisp2_dma_area    * const dma_mem)
{
  struct _STGFX2DriverData *drvdata;
  struct _STGFX2DeviceData *devdata;
  CoreGraphicsDevice       *device;
  GraphicsDeviceInfo       *device_info;
  int                       offset;

  drvdata = container_of (stdrv, struct _STGFX2DriverData, stdrv);
  devdata = container_of (stdev, struct _STGFX2DeviceData, stdev);

  device = drvdata->device;
  device_info = devdata->device_info;

  if (device_info->limits.surface_byteoffset_alignment < alignment)
    device_info->limits.surface_byteoffset_alignment = alignment;

  /* but else it's relatively simple, just reserve the memory... */
  offset = dfb_gfxcard_reserve_memory (device, dma_mem->size);
  if (offset <= 0)
      return -ENOMEM;

  dma_mem->base = dfb_gfxcard_memory_physical (device, offset);
  dma_mem->memory = dfb_gfxcard_memory_virtual (device, offset);

  return 0;
}

void
bdisp2_free_memory_os (struct stm_bdisp2_driver_data * const stdrv,
                       struct stm_bdisp2_device_data * const stdev,
                       struct stm_bdisp2_dma_area    * const dma_mem)
{
  /* nothing to do, we can't free it (and don't need to, as the master
     process is shutting down anyway */
}

#ifdef BDISP2_PRINT_NODE_WAIT_TIME
struct _my_timer_state {
  struct timeval start;
  struct timeval end;
};

void *
bdisp2_wait_space_start_timer_os (void)
{
  struct _my_timer_state *state;

  state = D_CALLOC (1, sizeof (*state));
  if (state)
    gettimeofday (&state->start, NULL);

  return state;
}

void
bdisp2_wait_space_end_timer_os (void *handle)
{
  if (handle)
    {
      struct _my_timer_state * const state = handle;

      gettimeofday (&state->end, NULL);
      timersub (&state->end, &state->start, &state->end);
      D_INFO ("BDisp/BLT: had to wait for space %lu.%06lus\n",
              state->end.tv_sec, state->end.tv_usec);

      D_FREE (state);
  }
}
#endif /* BDISP2_PRINT_NODE_WAIT_TIME */

int
bdisp2_push_nodes_os (const struct stm_bdisp2_driver_data * const stdrv,
                      struct stm_bdisp2_device_data * const stdev,
                      bool push_and_emit)
{
  struct push_nodes_cmd iotclcmd;

  struct _STGFX2DriverData * const drvdata
    = container_of (stdrv, struct _STGFX2DriverData, stdrv);
  struct _STGFX2DeviceData * const devdata
    = container_of (stdev, struct _STGFX2DeviceData, stdev);


  iotclcmd.nodes_buf_p    = devdata->nodes_buf.buf_p;
  iotclcmd.nodes_buf_len  = devdata->nodes_buf.next_elt;
  iotclcmd.emit           = push_and_emit;
  /*
   * printf ("%s BDisp/BLT:PUSH_NODES nodes_buf_p %p, nodes_buf_len %d  emit %d\n",__func__,
                iotclcmd.nodes_buf_p,iotclcmd.nodes_buf_len,iotclcmd.emit);
  */

  if (ioctl (drvdata->fd_gfx, STM_BLITTER_PUSH_NODES,&iotclcmd) < 0)
  {
   D_PERROR ("BDisp/BLT: STM_BLITTER_PUSH_NODES failed!\n" );
   return errno;
   }

  return 0;
}

void *
bdisp2_get_new_node (struct stm_bdisp2_driver_data        * const stdrv,
                     struct stm_bdisp2_device_data        * const stdev)
{
  unsigned char * new_node_p;
  struct _STGFX2DeviceData * const devdata
    = container_of (stdev, struct _STGFX2DeviceData, stdev);

  /* One element of the nodes buffer = size of node + node */
  new_node_p = devdata->nodes_buf.buf_p + devdata->nodes_buf.next_elt
               + sizeof(uint32_t);

  return (void *) (new_node_p);
}

void
bdisp2_finish_node (struct stm_bdisp2_driver_data       * const stdrv,
                    struct stm_bdisp2_device_data       * const stdev,
                    const void                          * const node,
                    const int                           node_size)
{
  unsigned char * elt_p;

  struct _STGFX2DeviceData * const devdata
    = container_of (stdev, struct _STGFX2DeviceData, stdev);

   D_ASSERT (node_size <= sizeof(struct _BltNode_Complete));

  /* Add the node size as header of the node
   * (each element of the nodes buffer = size of node + node) */

  elt_p = devdata->nodes_buf.buf_p + devdata->nodes_buf.next_elt;

  memcpy(elt_p,&node_size, sizeof(uint32_t));

  devdata->nodes_buf.next_elt += sizeof(uint32_t) + node_size;

  /* Update for next element */
  if((devdata->nodes_buf.next_elt + sizeof(struct _BltNode_Complete) + sizeof(uint32_t))
      > devdata->nodes_buf.buf_len)
  {

   if (bdisp2_push_nodes_os(stdrv,stdev,false) < 0)
     return;

   devdata->nodes_buf.next_elt = 0;
   return;
  }

  return;
}

static void
bdisp_set_dfb_features (const struct bdisp2_features *features,
                        struct _bdisp_dfb_features   *dfb_features)
{
  enum stm_blitter_surface_drawflags_e bdisp2_drawflags = features->drawflags;
  enum stm_blitter_surface_blitflags_e bdisp2_blitflags = features->blitflags;

  dfb_features->dfb_drawflags = DSDRAW_NOFX;
  dfb_features->dfb_blitflags = DSBLIT_NOFX;
  dfb_features->dfb_renderopts = DSRO_NONE;

  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_DST_COLORKEY))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_DST_COLORKEY;
      dfb_features->dfb_drawflags |= DSDRAW_DST_COLORKEY;
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_XOR))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_XOR;
      dfb_features->dfb_drawflags |= DSDRAW_XOR;
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_GRADIENT))
    {
      /* FIXME: STM_BLITTER_SDF_GRADIENT */
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_BLEND))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_BLEND;
      dfb_features->dfb_drawflags |= DSDRAW_BLEND;
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_SRC_PREMULTIPLY))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_SRC_PREMULTIPLY;
      dfb_features->dfb_drawflags |= DSDRAW_SRC_PREMULTIPLY;
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_DST_PREMULTIPLY))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_DST_PREMULTIPLY;
      dfb_features->dfb_drawflags |= DSDRAW_DST_PREMULTIPLY;
    }
  if (D_FLAGS_IS_SET (bdisp2_drawflags, STM_BLITTER_SDF_ANTIALIAS))
    {
      bdisp2_drawflags &= ~STM_BLITTER_SDF_ANTIALIAS;
      dfb_features->dfb_renderopts |= DSRO_ANTIALIAS;
    }

  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_ALL_IN_FIXED_POINT))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_ALL_IN_FIXED_POINT;
#if HAVE_DECL_DSBLIT_FIXEDPOINT
      dfb_features->dfb_blitflags |= DSBLIT_FIXEDPOINT;
#endif /* HAVE_DECL_DSBLIT_FIXEDPOINT */
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_SRC_COLORIZE))
    {
      /* FIXME: STM_BLITTER_SBF_SRC_COLORIZE is currently implemented to
         behave like DSBLIT_COLORIZE, but according to the API documentation
         it should behave more like DSBLIT_SRC_COLORMATRIX. */
      bdisp2_blitflags &= ~STM_BLITTER_SBF_SRC_COLORIZE;
      dfb_features->dfb_blitflags |= DSBLIT_COLORIZE;
      //dfb_features->dfb_blitflags |= DSBLIT_SRC_COLORMATRIX;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_SRC_COLORKEY))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_SRC_COLORKEY;
      dfb_features->dfb_blitflags |= DSBLIT_SRC_COLORKEY;
      /* FIXME: extended is not implemented */
      //dfb_features->dfb_blitflags |= DSBLIT_SRC_COLORKEY_EXTENDED;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_DST_COLORKEY))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_DST_COLORKEY;
      dfb_features->dfb_blitflags |= DSBLIT_DST_COLORKEY;
      /* FIXME: extended is not implemented */
      //dfb_features->dfb_blitflags |= DSBLIT_DST_COLORKEY_EXTENDED;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_XOR))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_XOR;
      dfb_features->dfb_blitflags |= DSBLIT_XOR;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_BLEND_ALPHACHANNEL))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_BLEND_ALPHACHANNEL;
      dfb_features->dfb_blitflags |= DSBLIT_BLEND_ALPHACHANNEL;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_BLEND_COLORALPHA))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_BLEND_COLORALPHA;
      dfb_features->dfb_blitflags |= DSBLIT_BLEND_COLORALPHA;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_SRC_PREMULTCOLOR))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_SRC_PREMULTCOLOR;
      dfb_features->dfb_blitflags |= DSBLIT_SRC_PREMULTCOLOR;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_SRC_PREMULTIPLY))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_SRC_PREMULTIPLY;
      dfb_features->dfb_blitflags |= DSBLIT_SRC_PREMULTIPLY;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_DST_PREMULTIPLY))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_DST_PREMULTIPLY;
      dfb_features->dfb_blitflags |= DSBLIT_DST_PREMULTIPLY;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_READ_SOURCE2))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_READ_SOURCE2;
#if HAVE_DECL_DSBLIT_SOURCE2
      dfb_features->dfb_blitflags |= DSBLIT_SOURCE2;
#endif
    }

  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_FLIP_HORIZONTAL))
    dfb_features->dfb_blitflags |= DSBLIT_FLIP_HORIZONTAL;
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_FLIP_VERTICAL))
    dfb_features->dfb_blitflags |= DSBLIT_FLIP_VERTICAL;
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_ROTATE90))
    dfb_features->dfb_blitflags |= DSBLIT_ROTATE90;
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_ROTATE180))
    dfb_features->dfb_blitflags |= DSBLIT_ROTATE180;
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_ROTATE270))
    dfb_features->dfb_blitflags |= DSBLIT_ROTATE270;
  bdisp2_blitflags &= ~(STM_BLITTER_SBF_FLIP_HORIZONTAL
                        | STM_BLITTER_SBF_FLIP_VERTICAL
                        | STM_BLITTER_SBF_ROTATE90
                        | STM_BLITTER_SBF_ROTATE180
                        | STM_BLITTER_SBF_ROTATE270
                       );

  if (D_FLAGS_ARE_SET (bdisp2_blitflags, (STM_BLITTER_SBF_DEINTERLACE_BOTTOM
                                          | STM_BLITTER_SBF_DEINTERLACE_TOP)))
    {
      bdisp2_blitflags &= ~(STM_BLITTER_SBF_DEINTERLACE_BOTTOM
                            | STM_BLITTER_SBF_DEINTERLACE_TOP);
      /* FIXME: deinterlace is not implemented */
      //dfb_features->dfb_blitflags |= DSBLIT_DEINTERLACE;
    }

  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_ANTIALIAS))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_ANTIALIAS;
      dfb_features->dfb_renderopts |= DSRO_ANTIALIAS;
    }
  if (D_FLAGS_ARE_SET (bdisp2_blitflags, STM_BLITTER_SBF_COLORMASK))
    {
      bdisp2_blitflags &= ~STM_BLITTER_SBF_COLORMASK;
      /* FIXME: not implemented */
      //dfb_features->dfb_renderopts |= DSRO_WRITE_MASK_BITS;
    }

  /* stuff that has no corresponding DirectFB API */
  bdisp2_drawflags &= ~STM_BLITTER_SDF_GRADIENT;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_VC1RANGE_LUMA;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_VC1RANGE_CHROMA;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_FLICKER_FILTER;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_STRICT_INPUT_RECT;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_STRICT_FILTER_USAGE;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_NO_FILTER;
  bdisp2_blitflags &= ~STM_BLITTER_SBF_BOUNDARY_BYPASS;
  /* stuff we always support (that isn't represented in the core driver and is
     DirectFB specific). */
  dfb_features->dfb_renderopts |= (DSRO_SMOOTH_UPSCALE
                                   | DSRO_SMOOTH_DOWNSCALE);
  dfb_features->dfb_blitflags |= DSBLIT_INDEX_TRANSLATION;
  dfb_features->dfb_renderopts |= DSRO_MATRIX;

  if (bdisp2_drawflags)
    D_WARN ("BDisp/BLT: unhandled core driver draw flags: %.08x\n",
            bdisp2_drawflags);
  if (bdisp2_blitflags)
    D_WARN ("BDisp/BLT: unhandled core driver blit flags: %.08x\n",
            bdisp2_blitflags);


  {
  char * const drawflags_str = stgfx2_get_dfb_drawflags_string (dfb_features->dfb_drawflags);
  char * const blitflags_str = stgfx2_get_dfb_blitflags_string (dfb_features->dfb_blitflags);
  char * const renderopts_str = stgfx2_get_dfb_renderoptions_string (dfb_features->dfb_renderopts);

  D_INFO("BDisp/BLT: supported DirectFB draw flags: %s\n", drawflags_str);
  D_INFO("BDisp/BLT: supported DirectFB blit flags: %s\n", blitflags_str);
  D_INFO("BDisp/BLT: supported DirectFB render opt: %s\n", renderopts_str);

  free (renderopts_str);
  free (blitflags_str);
  free (drawflags_str);
  }
}

DFBResult
bdisp_aq_initialize (CoreGraphicsDevice       * const device,
                     GraphicsDeviceInfo       * const device_info,
                     struct _STGFX2DriverData * const drvdata,
                     struct _STGFX2DeviceData * const devdata)
{
  DFBResult res;
  long page_size;
  struct get_bdisp2_features_cmd cmd;
  int bdisp2_drv_ver = -1;

  if (ioctl(drvdata->fd_gfx,STM_BLITTER_GET_DRV_VER,&bdisp2_drv_ver) < 0)
  {
    D_PERROR ("BDisp/BLT: STM_BLITTER_GET_DRV_VER returns %d error\n",-errno);
    return -EINVAL;
  }

  if (bdisp2_drv_ver != STM_BSISP2_DRV_VERSION)
  {
   D_PERROR ("BDisp/BLT:Stgfx2 was not compiled against \
             bdisp2 driver version %d \n",STM_BSISP2_DRV_VERSION);
   return -EINVAL;
  }

  if ((page_size = (long)sysconf(_SC_PAGESIZE)) < 0)
    return DFB_BUG;

  res = errno2result (bdisp2_initialize (&drvdata->stdrv, &devdata->stdev));
  if (res == DFB_OK)
  {
    if (ioctl(drvdata->fd_gfx,STM_BLITTER_GET_FEATURES,&cmd) < 0)
    {
      D_PERROR ("BDisp/BLT: STM_BLITTER_GET_FEATURES returns %d error\n",-errno);
      return -EINVAL;
    }

    devdata->stdev.features = cmd.features;

    bdisp_set_dfb_features (&devdata->stdev.features,
                            &devdata->dfb_features);

  }
  return res;
}


void
bdisp_aq_EngineReset (void * const driver_data,
                      void * const device_data)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  bdisp2_engine_reset (stdrv, stdev);
}

DFBResult
bdisp_aq_EngineSync (void * const driver_data,
                     void * const device_data)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  return errno2result (bdisp2_engine_sync (stdrv, stdev));
}

void
bdisp_aq_EmitCommands (void * const driver_data,
                       void * const device_data)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data       * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data       * const stdev = &devdata->stdev;

  if (bdisp2_push_nodes_os(stdrv,stdev,true) < 0)
   {
	  return;
   }
  devdata->nodes_buf.next_elt = 0;
}

void
bdisp_aq_GetSerial (void               * const driver_data,
                    void               * const device_data,
                    CoreGraphicsSerial * const dfb_serial)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct get_serial_cmd cmd;

  if (ioctl (drvdata->fd_gfx, STM_BLITTER_GET_SERIAL,&cmd) < 0)
    D_PERROR ("BDisp/BLT: STM_BLITTER_GET_SERIAL failed!\n" );

  dfb_serial->generation = cmd.serial >> 32;
  dfb_serial->serial     = cmd.serial & 0xffffffff;
}

DFBResult
bdisp_aq_WaitSerial (void                     * const driver_data,
                     void                     * const device_data,
                     const CoreGraphicsSerial * const dfb_serial)
{
  struct wait_cmd cmd;
  struct _STGFX2DriverData * const drvdata = driver_data;
  int ret = 0;

  cmd.serial = ((stm_blitter_serial_t) dfb_serial->generation) << 32;
  cmd.serial |= dfb_serial->serial;
  cmd.wait = STM_BLITTER_W_SERIAL;

  while (ioctl (drvdata->fd_gfx, STM_BLITTER_SYNC,&cmd) < 0)
     {
       if (errno == EINTR)
         continue;

       ret = errno;
       D_PERROR ("BDisp/BLT: STM_BLITTER_SYNC failed!\n" );

       break;
     }

  return ret;
}

bool
bdisp_aq_FillRectangle (void         * const driver_data,
                        void         * const device_data,
                        DFBRectangle * const drect)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t dst = { .position = { .x = drect->x,
                                           .y = drect->y },
                             .size = { .w = drect->w,
                                       .h = drect->h } };

  if (stdrv->funcs.fill_rect (stdrv, stdev, &dst))
    return false;

  return true;
}

bool
bdisp_aq_DrawRectangle (void         * const driver_data,
                        void         * const device_data,
                        DFBRectangle * const drect)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t dst = { .position = { .x = drect->x,
                                           .y = drect->y },
                             .size = { .w = drect->w,
                                       .h = drect->h } };

  if (stdrv->funcs.draw_rect (stdrv, stdev, &dst))
    return false;

  return true;
}

bool
bdisp_aq_Blit (void         * const driver_data,
               void         * const device_data,
               DFBRectangle * const srect,
               int           dx,
               int           dy)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t src = { .position = { .x = srect->x,
                                           .y = srect->y },
                             .size = { .w = srect->w,
                                       .h = srect->h } };
  stm_blitter_point_t dst_pt = { .x = dx,
                                 .y = dy };

  if (stdrv->funcs.blit (stdrv, stdev, &src, &dst_pt))
    return false;

  return true;
}

bool
bdisp_aq_StretchBlit (void         * const driver_data,
                      void         * const device_data,
                      DFBRectangle * const srect,
                      DFBRectangle * const drect)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t src = { .position = { .x = srect->x,
                                           .y = srect->y },
                             .size = { .w = srect->w,
                                       .h = srect->h } };
  stm_blitter_rect_t dst = { .position = { .x = drect->x,
                                           .y = drect->y },
                             .size = { .w = drect->w,
                                       .h = drect->h } };

  if (stdrv->funcs.stretch_blit (stdrv, stdev, &src, &dst))
    return false;

  return true;
}

bool
bdisp_aq_Blit2 (void         * const driver_data,
                void         * const device_data,
                DFBRectangle * const srect,
                int           dx,
                int           dy,
                int           sx2,
                int           sy2)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t src1 = { .position = { .x = srect->x,
                                            .y = srect->y },
                              .size = { .w = srect->w,
                                        .h = srect->h } };
  stm_blitter_point_t src2_pt = { .x = sx2, .y = sy2 };
  stm_blitter_point_t dst_pt  = { .x = dx, .y = dy };

  if (stdrv->funcs.blit2 (stdrv, stdev, &src1, &src2_pt, &dst_pt))
    return false;

  return true;
}

extern const enum stm_blitter_surface_format_e dspf_to_stm_blit[];
bool
bdisp_aq_StretchBlit_RLE (void                        * const driver_data,
                          void                        * const device_data,
                          unsigned long                src_address,
                          unsigned long                src_length,
                          const CoreSurfaceBufferLock * const dst,
                          const DFBRectangle          * const drect)
{
  struct _STGFX2DriverData * const drvdata = driver_data;
  struct _STGFX2DeviceData * const devdata = device_data;

  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t rect = { .position = { .x = drect->x,
                                            .y = drect->y },
                              .size = { .w = drect->w,
                                        .h = drect->h } };

  enum stm_blitter_surface_format_e dst_format;

  dst_format = dspf_to_stm_blit[DFB_PIXELFORMAT_INDEX (dst->buffer->format)];

  if (bdisp2_stretch_blit_RLE (stdrv, stdev, src_address, src_length,
                               dst->phys, dst_format, dst->pitch,
                               &rect))
    return false;

  return true;
}

void
_bdisp_aq_RGB32_init (struct _STGFX2DriverData * const drvdata,
                      struct _STGFX2DeviceData * const devdata,
                      uint32_t                  blt_tba,
                      uint16_t                  pitch,
                      DFBRectangle             * const drect)
{
  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t dst = { .position = { .x = drect->x,
                                           .y = drect->y },
                             .size = { .w = drect->w,
                                       .h = drect->h } };

  bdisp2_RGB32_init (stdrv, stdev, blt_tba, pitch, &dst);

  bdisp_aq_EmitCommands(drvdata,devdata);
}

void
_bdisp_aq_RGB32_fixup (struct _STGFX2DriverData * const drvdata,
                       struct _STGFX2DeviceData * const devdata,
                       uint32_t                  blt_tba,
                       uint16_t                  pitch,
                       DFBRectangle             * const drect)
{
  struct stm_bdisp2_driver_data * const stdrv = &drvdata->stdrv;
  struct stm_bdisp2_device_data * const stdev = &devdata->stdev;

  stm_blitter_rect_t dst = { .position = { .x = drect->x,
                                           .y = drect->y },
                             .size = { .w = drect->w,
                                       .h = drect->h } };

  bdisp2_RGB32_fixup (stdrv, stdev, blt_tba, pitch, &dst);

  bdisp_aq_EmitCommands(drvdata,devdata);

  bdisp2_engine_sync_os(stdrv,stdev);
}
