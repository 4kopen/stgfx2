/*
* This file is part of stgfx2
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
* License type: LGPLv2.1
*
* stgfx2 is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* stgfx2 is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*/


#ifndef __STGFX2_FEATURES_H__
#define __STGFX2_FEATURES_H__


/************************** implementation details **************************/
/* the following are basically implementation details of this driver and you
   should not need to touch them. */

/* we used to accelerate DRAWLINE just for being able to draw horizontal
   and vertical lines in hardware, which is what happens in a
   cairo/gtk/webkit environment. But nowadays DirectFB is intelligent enough
   to draw rectangles instead in this case, so that code was removed. */
#define STGFX2_VALID_DRAWINGFUNCTIONS (0 \
                                       | DFXL_FILLRECTANGLE \
                                       | DFXL_DRAWRECTANGLE \
                                      )

#define STGFX2_VALID_BLITTINGFUNCTIONS (DFXL_BLIT \
                                        | DFXL_STRETCHBLIT \
                                        | DFXL_BLIT2)

#endif /* __STGFX2_FEATURES_H__ */
