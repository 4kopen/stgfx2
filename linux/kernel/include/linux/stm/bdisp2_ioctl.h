/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __BDISP2_IOCTL_H__
#define __BDISP2_IOCTL_H__

#include <linux/ioctl.h>

#include <linux/stm/blitter.h>

#include <linux/stm/bdisp2_features.h>

#define STM_GFXDRV_NODES_BUF_SZ 11*1024

enum stm_bdisp2_wait_e {
	STM_BLITTER_W_IDLE,
	STM_BLITTER_W_SERIAL,
};

struct push_nodes_cmd {
	unsigned char *nodes_buf_p;
	int            nodes_buf_len;
	int            emit;
};

struct get_serial_cmd {
	stm_blitter_serial_t serial;
};

struct wait_cmd {
	enum stm_bdisp2_wait_e      wait;
	stm_blitter_serial_t        serial;
};

struct get_bdisp2_features_cmd {
	struct bdisp2_features      features;
};


#define STM_BLITTER_GET_BLTLOAD    _IOR ('B', 0x1a, unsigned long)
#define STM_BLITTER_GET_BLTLOAD2   _IOR ('B', 0x1a, unsigned long long)
#define STM_BLITTER_SYNC           _IOWR ('B', 0x5, struct wait_cmd) /* wait for blitter idle */
#define STM_BLITTER_GET_SERIAL     _IOW ('B', 0x2a, struct get_serial_cmd) /*get serial number of the last
                                                                           node in the node list */
#define STM_BLITTER_PUSH_NODES     _IOR ('B', 0x2b, struct push_nodes_cmd)
#define STM_BLITTER_GET_FEATURES   _IOR ('B', 0x2c, struct get_bdisp2_features_cmd)
#define STM_BLITTER_GET_DRV_VER    _IOR ('B', 0x2d, int)

#endif /* __BDISP2_IOCTL_H__ */
