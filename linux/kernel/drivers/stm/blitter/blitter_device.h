/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __STM_BLITTER_DEVICE_H__
#define __STM_BLITTER_DEVICE_H__

#include <linux/device.h>
#include "class.h"

#define STM_BLITTER_MAX_DEVICES 8

int stm_blitter_register_device(struct device                     *dev,
				char                              *device_name,
				const struct stm_blitter_file_ops *fops,
				void                              *data);
int stm_blitter_unregister_device(void *data);


int stm_blitter_validate_address(stm_blitter_t *blitter,
				 unsigned long  start,
				 unsigned long  end);


#endif /* __STM_BLITTER_DEVICE_H__ */
