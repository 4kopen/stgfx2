/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __STM_BLIT_DEBUG_H__
#define __STM_BLIT_DEBUG_H__


#ifdef __KERNEL__
#include <linux/bug.h>
/*
 * STM_BLITTER_ASSERT has to be used to track only development bug likely
 * impossible situations and not wrong parameter. Could be evaluated to void
 * if debug is disabled so has to be used with and braces when needed.
 */
#  ifndef STM_BLITTER_ASSERT
#    define STM_BLITTER_ASSERT(cond)   BUG_ON(!(cond))
#  endif
#endif /* __KERNEL__ */

#define stm_blitter_magic(str) \
	(0                                                                   \
	 | ((((str)[sizeof(str) - 1]) | (((str)[sizeof(str) / 2]))) << 24)   \
	 | ((((str)[sizeof(str) / 3]) | (((str)[sizeof(str) / 4]))) << 16)   \
	 | ((((str)[sizeof(str) / 5]) | (((str)[sizeof(str) / 6]))) <<  8)   \
	 | ((((str)[sizeof(str) / 7]) | (((str)[sizeof(str) / 8]))) <<  0)   \
	)

#define stm_blitter_magic_check(ptr, mag) \
	({ \
		if ((ptr) == NULL)                                                   \
			stm_blit_printe("%s: Invalid parameter : " #ptr, __func__);  \
		if ((ptr) != NULL && (ptr)->magic != stm_blitter_magic(#mag))        \
			stm_blit_printe("%s: Corrupted object  : " #ptr, __func__);  \
		( ((ptr) == NULL)?false:                                             \
		  (((ptr)->magic != stm_blitter_magic(#mag))?false:                  \
		   true) );                                                          \
	})

/*
 * Based on BUG_ON for null pointer check, as by construction it is called
 * after object creation so its impossible to reach this kind of situation
 * unless implementation bug.
 */
#define stm_blitter_magic_set(ptr, mag) \
	({ \
		STM_BLITTER_ASSERT((ptr) != NULL);      \
		(ptr)->magic = stm_blitter_magic(#mag); \
	})

#define stm_blitter_magic_clear(ptr) \
	({ \
		STM_BLITTER_ASSERT((ptr) != NULL);               \
		if ((ptr)->magic == 0)                           \
			/* Object is being destroyed any way */  \
			/* just print a message. */              \
			stm_blit_printe("%s: Invalid parameter " \
			                #ptr, __func__);         \
		(ptr)->magic = 0;                                \
	})


#endif /* __STM_BLIT_DEBUG_H__ */
