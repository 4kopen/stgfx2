/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#ifndef __BLITTER_OS_H__
#define __BLITTER_OS_H__

#ifdef __KERNEL__

#include <linux/slab.h>

#define stm_blitter_allocate_mem(size) kmalloc(size, GFP_KERNEL)
#define stm_blitter_free_mem(ptr)      kfree(ptr)

#else /* __KERNEL__ */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>

#define stm_blitter_allocate_mem(size) malloc(size)
#define stm_blitter_free_mem(ptr)      free(ptr)

#endif /* __KERNEL__ */


#endif /* __BLITTER_OS_H__ */
