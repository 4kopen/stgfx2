/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/


#ifndef __BDISPII_DRIVER_FEATURES_H__
#define __BDISPII_DRIVER_FEATURES_H__


/***************************** some debug/stats *****************************/
/* should we printf() the time we had to wait for an entry in the nodelist
   to be available? */
#undef BDISP2_PRINT_NODE_WAIT_TIME

/* If this is defined, the CLUT will be uploaded only once into the BDisp
   and will be assumed not to change anytime after outside our control.
   So if multiple applications using different CLUTs on different BDisp AQs
   are active (or if somebody is using the kernel interface in parallel) the
   CLUT feature will not work correctly. This feature is therefore disabled
   by default. In earlier versions of this driver we gained a little
   performance increase, but this is not the case anymore, so there's no
   point in enabling this option... */
#undef BDISP2_CLUT_UNSAFE_MULTISESSION

#define STM_BSISP2_DRV_VERSION 4

#endif /* __STGFX2_FEATURES_H__ */
