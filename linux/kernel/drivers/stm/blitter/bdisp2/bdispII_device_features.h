/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/


#ifndef __BDISPII_DEVICE_FEATURES_H__
#define __BDISPII_DEVICE_FEATURES_H__

#include <linux/types.h>

enum stm_blitter_device_type {
	STM_BLITTER_VERSION_STiH418               = 17,
};

/* This structure is used to describe hardware configuration parameter
   Along with device implementation. */
struct stm_plat_blit_data {
	/* 100 */
	enum stm_blitter_device_type  device_type; /* BDisp implementation,
						      keep first for old lib
						      compatibility */
	/* 104 */
	unsigned int  nb_aq;                /* Nb AQ available for this host */
	/* 108 */
	unsigned int  nb_cq;                /* Nb CQ available for this host */
	/* 112 */
	unsigned int  line_buffer_length;   /* resize/filtering spans size*/
	/* 116 */
	unsigned int  mb_buffer_length;     /* mb line:should be equal to line
					       delay unless H/W bug (h416c1) */
	/* 120 */
	unsigned int  rotate_buffer_length; /* rotation spans max width */

	/* 124 */
	unsigned int  clk_ip_freq;

	/* 128 */
	unsigned char device_name[20];      /* /dev/"device_name" that will
					       be created */
	/* 148*/
	unsigned int hw_instance_id; 		/*bdisp hw instance : bdisp0 or 1 */
};
#endif /* __BDISPII_DEVICE_FEATURES_H__ */
