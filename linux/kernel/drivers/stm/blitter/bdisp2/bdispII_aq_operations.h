/*
* This file is dual licensed, either GPLv2.0
* or LGPLv2.1, at your option.
*
* Copyright 2015, STMicroelectronics - All Rights Reserved
*
********************************************************************************
*
* License type: GPLv2.0
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* version 2 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with it. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
* Alternatively, this may be distributed under the terms of
* LGPLv2.1, in which case the following provisions apply instead of the ones
* mentioned above :
*
********************************************************************************
*
* This is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public License
* version 2.1 as published by the Free Software Foundation.
*
* It is distributed in the hope that it will be
* useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library. If not, see
* <http://www.gnu.org/licenses/>.
*
********************************************************************************
*
*/

#ifndef __BDISPII_AQ_OPERATIONS_H__
#define __BDISPII_AQ_OPERATIONS_H__


#include <linux/stm/blitter.h>

struct stm_bdisp2_driver_data;
struct stm_bdisp2_device_data;

struct bdisp2_funcs {
	int (*fill_rect) (struct stm_bdisp2_driver_data       *stdrv,
			          struct stm_bdisp2_device_data       *stdev,
			          const stm_blitter_rect_t            *dst);

	int (*draw_rect) (struct stm_bdisp2_driver_data       *stdrv,
			          struct stm_bdisp2_device_data       *stdev,
			          const stm_blitter_rect_t            *dst);

	int (*blit) (struct stm_bdisp2_driver_data  *stdrv,
			     struct stm_bdisp2_device_data  *stdev,
			     const stm_blitter_rect_t       *src,
			     const stm_blitter_point_t      *dst_pt);

	int (*stretch_blit) (struct stm_bdisp2_driver_data  *stdrv,
			     struct stm_bdisp2_device_data  *stdev,
			     const stm_blitter_rect_t       *src,
			     const stm_blitter_rect_t       *dst);

	int (*blit2) (struct stm_bdisp2_driver_data *stdrv,
			     struct stm_bdisp2_device_data  *stdev,
			     const stm_blitter_rect_t       *src1,
			     const stm_blitter_point_t      *src2_pt,
			     const stm_blitter_point_t      *dst_pt);
};


#endif /* __BDISPII_AQ_OPERATIONS_H__ */
