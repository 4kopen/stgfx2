/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/

#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/pm.h>
#include <linux/pm_runtime.h>
#include <linux/io.h>

#include <linux/delay.h>

#include <linux/of.h>
#include "device/device_dt.h"

#include "bdisp2/bdisp2_os.h"
#include "blit_debug.h"

#include "bdisp2/bdispII_aq.h"
#include "bdisp2/bdispII_fops.h"
#include "blitter_device.h"
#include "class.h"


static int __init
stm_blit_memory_request(struct platform_device  *pdev,
			const char              *mem_name,
			struct resource        **mem_region,
			void __iomem           **io_base)
{
	struct resource *resource;

	resource = platform_get_resource_byname(pdev,
						IORESOURCE_MEM, mem_name);
	*io_base = devm_ioremap_resource(&pdev->dev, resource);
	if (IS_ERR(*io_base))
		return PTR_ERR(*io_base);

	*mem_region = resource;

	dev_dbg(&pdev->dev, "Base address is 0x%p\n", *io_base);

	return 0;
}

static int __init
stm_bdisp2_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct stm_plat_blit_data *plat_data;
	struct stm_bdisp2_config *bc;
	int  i;
	int  res;

	stm_blit_printd(0, "%s (pdev %p, dev %p)\n", __func__, pdev, dev);

	if (pdev->dev.of_node)
		stm_blit_dt_get_pdata(pdev);

	plat_data = (struct stm_plat_blit_data *)pdev->dev.platform_data;

	if (!plat_data) {
		res = -ENOMEM;
		goto out;
	}

	bc = devm_kzalloc(dev, sizeof(*bc), GFP_KERNEL);
	if (!bc) {
		stm_blit_printe(
			"Can't allocate memory for device description\n");
		res = -ENOMEM;
		goto out;
	}
	spin_lock_init(&bc->register_lock);


	res = stm_blit_memory_request(pdev, "bdisp-io",
				      &bc->mem_region, &bc->io_base);
	if (res)
		goto out;

	bc->hw_instance_id = plat_data->hw_instance_id;

	/* clock stuff */
	bc->clk_ip = devm_clk_get(dev, "bdisp0");
	if (IS_ERR(bc->clk_ip))
		bc->clk_ip = NULL;

	bc->clk_ip_freq = plat_data->clk_ip_freq;
	if (bc->clk_ip_freq) {
		res = clk_set_rate(bc->clk_ip, bc->clk_ip_freq);
		if (res)
			goto out;
	}

	for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
		static const char * const irq_name[] = { "bdisp-aq1",
							 "bdisp-aq2",
							 "bdisp-aq3",
							 "bdisp-aq4" };
		struct resource * const resource =
			platform_get_resource_byname(pdev, IORESOURCE_IRQ,
						     irq_name[i]);
		if (!resource) {
			stm_blit_printi("AQ(%s) not found\n", irq_name[i]);
			continue;
		}

		stm_blit_printd(0, "AQ(%d) IRQ: %u\n", i, resource->start);

		res = stm_bdisp2_aq_init(&bc->aq_data[i], i,
					 resource->start,
					 plat_data,
					 bc);
		if (res < 0)
			goto out_interrupts;

#if defined(CONFIG_PM_RUNTIME)
		bc->aq_data[i].dev = &pdev->dev;
#endif

		stm_blitter_register_device(dev,
					    plat_data->device_name,
					    &bdisp2_aq_fops,
					    &bc->aq_data[i]);
	}

	platform_set_drvdata(pdev, bc);

	/* The device is active already (clocks are running, etc. - tell the PM
	   subsystem about this. */
	pm_runtime_set_active(&pdev->dev);
	/* allow the PM subsystem to call our callbacks */
	pm_runtime_enable(&pdev->dev);
	/* Now that we have probed - we can suspend until we have work */
	pm_runtime_suspend(&pdev->dev);
	/* Set the delay to suspend the system */
	pm_runtime_set_autosuspend_delay(&pdev->dev, 50);
	/* Activate the delay mechanism */
	pm_runtime_use_autosuspend(&pdev->dev);

	return 0;

out_interrupts:
	while (i--) {
		stm_blitter_unregister_device(&bc->aq_data[i]);
		stm_bdisp2_aq_release(&bc->aq_data[i]);
	}
out:
	return res;
}

static int __exit
stm_bdisp2_remove(struct platform_device *pdev)
{
	struct stm_bdisp2_config *bc;

	stm_blit_printd(0, "%s\n", __func__);

	/* this will cause us to be suspended */
	pm_runtime_put_sync(&pdev->dev);
	/* disallow PM callbacks */
	pm_runtime_disable(&pdev->dev);

	bc = platform_get_drvdata(pdev);
	if (bc != NULL) {
		int i;
		for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
			struct stm_bdisp2_aq * const aq = &bc->aq_data[i];
			if (!aq->irq)
				continue;

			stm_blitter_unregister_device(aq);
			stm_bdisp2_aq_release(aq);
		}
	}

	platform_set_drvdata(pdev, NULL);

	if (pdev->dev.of_node)
		stm_blit_dt_put_pdata(pdev);

	return 0;
}

#if defined(CONFIG_PM)
static int stm_bdisp2_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct stm_bdisp2_config *bc;
	int retval = 0;

	stm_blit_printd(0, "%s\n", __func__);

#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_suspended(dev)) {
		stm_blit_printd(0, "%s - Already suspended by Runtime PM\n", __func__);
		return retval;
	}
#endif

	bc = platform_get_drvdata(pdev);
	if (bc != NULL) {
		int i;
		for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
			struct stm_bdisp2_aq * const aq = &bc->aq_data[i];
			if (!aq->irq)
				continue;

			retval |= stm_bdisp2_aq_suspend(aq);
		}
	}

	return retval;
}

static int stm_bdisp2_rpm_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct stm_bdisp2_config *bc;
	int retval = 0;
	unsigned long irqmask;
	bool is_bdisp2_idle;

	stm_blit_printd(0, "%s\n", __func__);

	if (pm_runtime_suspended(dev)) {
		stm_blit_printd(0, "%s - Already suspended by Runtime PM\n", __func__);
		return retval;
	}

	bc = platform_get_drvdata(pdev);
	if (bc != NULL) {
		int i;
		for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
			struct stm_bdisp2_aq * const aq = &bc->aq_data[i];
			if (!aq->irq)
				continue;

			/* Get the bdisp2 status */
			is_bdisp2_idle = bdisp2_is_idle(&aq->stdrv);

			/* Get the interruptions status for the current queue */
			irqmask = bdisp2_get_reg(&aq->stdrv, BDISP_ITS) & (aq->lna_irq_bit
									   | aq->node_irq_bit);

			/* If blitter is still in use don't suspend, but update
			   last_busy so autosuspend is automatically rescheduled */
			if (is_bdisp2_idle && !irqmask) {
				retval |= stm_bdisp2_aq_suspend(aq);

				aq->stdrv.bdisp_suspended = 2;
				aq->stdrv.num_pending_requests--;
			} else {
				stm_blit_printd(0, "%s - Runtime PM suspend aborted, device still in use\n", __func__);

#if defined(CONFIG_PM_RUNTIME)
				/* Update to new last known busy time */
				pm_runtime_mark_last_busy(aq->dev);
#endif

				/* Return -EBUSY so that Runtime PM automatically reschedule a new suspend */
				retval |= -EBUSY;
			}
		}
	}

	return retval;
}

static int stm_bdisp2_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct stm_bdisp2_config *bc;
	int retval = 0;

	stm_blit_printd(0, "%s\n", __func__);

#ifdef CONFIG_PM_RUNTIME
	if (pm_runtime_suspended(dev)) {
		stm_blit_printd(0, "%s - Suspended by Runtime PM and should be waked by Runtime PM\n", __func__);
		return retval;
	}
#endif

	bc = platform_get_drvdata(pdev);
	if (bc != NULL) {
		int i;
		for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
			struct stm_bdisp2_aq * const aq = &bc->aq_data[i];
			if (!aq->irq)
				continue;

			retval |= stm_bdisp2_aq_resume(aq);
		}
	}

	return retval;
}

static int stm_bdisp2_rpm_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct stm_bdisp2_config *bc;
	int retval = 0;

	stm_blit_printd(0, "%s\n", __func__);

	bc = platform_get_drvdata(pdev);
	if (bc != NULL) {
		int i;
		for (i = 0; i < STM_BDISP2_MAX_AQs; ++i) {
			struct stm_bdisp2_aq * const aq = &bc->aq_data[i];
			if (!aq->irq)
				continue;

			retval |= stm_bdisp2_aq_resume(aq);
		}
	}

	return retval;
}

static const struct dev_pm_ops bdisp2_pm_ops = {
	.suspend         = stm_bdisp2_suspend,
	.resume          = stm_bdisp2_resume,
	.freeze          = stm_bdisp2_suspend,
	.thaw            = stm_bdisp2_resume,
	.poweroff        = stm_bdisp2_suspend,
	.restore         = stm_bdisp2_resume,
	.runtime_suspend = stm_bdisp2_rpm_suspend,
	.runtime_resume  = stm_bdisp2_rpm_resume,
	.runtime_idle    = NULL,
};

#define BDISP2_PM_OPS   (&bdisp2_pm_ops)
#else /* CONFIG_PM */
#define BDISP2_PM_OPS   NULL
#endif /* CONFIG_PM */

struct of_device_id stm_blitter_match[] = {
	{
	  .compatible = "st,bdispII-h418",
	  .data = (void *)STM_BLITTER_VERSION_STiH418
	},
	{},
};

static struct platform_driver __refdata stm_bdisp2_driver = {
	.driver.name = "stm-bdispII",
	.driver.owner = THIS_MODULE,
	.driver.pm = BDISP2_PM_OPS,
	.driver.of_match_table = of_match_ptr(stm_blitter_match),
	.probe = stm_bdisp2_probe,
	.remove = __exit_p(stm_bdisp2_remove),
};


static int __init
stm_bdisp2_init(void)
{
	int res;

	stm_blit_printd(0, "%s\n", __func__);

	stm_blitter_class_init(STM_BLITTER_MAX_DEVICES);

	/* This call is added here to allow setup of required
	   blitter counter for non unified loading */
	res = stm_blit_dt_init();
	if (res)
		return res;

	res = platform_driver_register(&stm_bdisp2_driver);

	if (res)
		stm_blit_dt_exit();

	return res;
}

static void __exit
stm_bdisp2_exit(void)
{
	stm_blit_printd(0, "%s\n", __func__);

	platform_driver_unregister(&stm_bdisp2_driver);

	stm_blit_dt_exit();

	stm_blitter_class_cleanup(STM_BLITTER_MAX_DEVICES);
}

MODULE_AUTHOR("André Draszik <andre.draszik@st.com");
MODULE_DESCRIPTION("STMicroelectronics BDispII driver");
MODULE_LICENSE("GPL");
MODULE_VERSION(KBUILD_VERSION);

module_init(stm_bdisp2_init);
module_exit(stm_bdisp2_exit);
