/************************************************************************
Copyright (C) 2003-2018 STMicroelectronics. All Rights Reserved.

This is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License version 2 as
published by the Free Software Foundation.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with it; see the file COPYING.  If not, write to the Free
Software Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307,
USA.

This Library may alternatively be licensed under a
proprietary license from STMicroelectronics.
************************************************************************/


#ifndef __STM_BLITTER_SURF_UTILS_H__
#define __STM_BLITTER_SURF_UTILS_H__

#include <linux/stm/blitter.h>


struct blit_fb_bpa2_test_surface {
  stm_blitter_surface_t *surf;
  void                  *priv;
};


void free_fb_bpa2_surf (struct blit_fb_bpa2_test_surface *surf);
struct blit_fb_bpa2_test_surface *
get_fb_or_bpa2_surf (stm_blitter_dimension_t       *dim,
                     stm_blitter_surface_address_t *buffer,
                     size_t                        *buffer_size,
                     unsigned long                 *pitch,
                     stm_blitter_surface_format_t  *fmt);


#endif /* __STM_BLITTER_SURF_UTILS_H__) */
